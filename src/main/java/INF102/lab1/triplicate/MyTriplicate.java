package INF102.lab1.triplicate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        Map<T, Integer> frequencyMap = new HashMap<>();

        for (T element : list) {
            // Get the current count of this element (default to 0 if not present)
            int count = frequencyMap.getOrDefault(element, 0);

            // Increment the count
            frequencyMap.put(element, count + 1);

            // Check if this element appears three times now
            if (frequencyMap.get(element) == 3) {
                return element;
            }
        }

        return null;
    }
}